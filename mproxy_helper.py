#!/usr/bin/env  python
# -*- coding:utf-8 -*-

import sys
import time
import contextlib

def paser(fin, fout):
    for line in fin:
        index = line.find('---[=[ @@')
        if index == -1:
            fout.write(line)
            continue;
        fout.write(line[:index]+line[index+1:])

if __name__ == "__main__":
    length = len(sys.argv)
    if length == 1:
        src_name = "mproxy.lua"
        dst_name = 'mproxy_new.lua'
    elif length == 2:
        src_name = sys.argv[1]
        dst_name = 'mproxy_new.lua'
    elif length == 3:
        src_name = sys.argv[1]
        dst_name = sys.argv[2]
    else:
        print 'argv error'
        exit()
    
    print src_name
    print dst_name
    with contextlib.nested(open(src_name,'r'), open(dst_name,'w')) as (fin, fout):
            paser(fin, fout)
