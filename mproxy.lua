--  1. maintains a connection pool.
--  2. slave-protection, only select statement can be executed on slaves.
--  3. support debug log.
--
-- @ Use --[=[@@ and log.xxx as reserved-word for pretreatment.
--]]

local commands    = require("proxy.commands")
local tokenizer   = require("proxy.tokenizer")
local auto_config = require("proxy.auto-config")

--- configuration of variable
local min_idle_connections = 4
local max_idle_connections = 8
--- end of configuration

if not proxy.global.balance then
    proxy.global.balance = {
        connect_index = 1,
        query_index = 1,
        disconnect_index = 1,
    }
end

---[=[ @@LOG assisted function for log
local log_level = 1
log = {
    level = { debug=1, info=2, warn=3, error=4 },
    funcs = { "debug", "info", "warn", "error" },
}

function log.log(level, m)
    if level >= log_level then
        local msg = "[" .. os.date("%Y-%m-%d %X") .."] "..
        log.funcs[level] .. ": " .. tostring(m)
        print(msg)
    end
end
for i, v in ipairs(log.funcs) do
    log[v] = function(m)
        log.log(log.level[v], m)
    end
end
--]=]


function idle_failsafe_rw()
    local backend_ndx = 0

    for i = 1, #proxy.global.backends do
        local s = proxy.global.backends[i]
        local conns = s.pool.users[proxy.connection.client.username]

        if conns.cur_idle_connections > 0 and
            s.state ~= proxy.BACKEND_STATE_DOWN and
            s.type == proxy.BACKEND_TYPE_RW then
            backend_ndx = i
            break
        end
    end
    return backend_ndx
end

function idle_ro()
    local max_conns = -1
    local max_conns_ndx = 0
    local index = proxy.global.balance.query_index
    local num = #proxy.global.backends
    for j = 1, num do
        if index > num then index = 1 end
        local s = proxy.global.backends[index]
        local conns = s.pool.users[proxy.connection.client.username]

        -- pick a slave which has some idling connections
        if s.type == proxy.BACKEND_TYPE_RO and
            s.state ~= proxy.BACKEND_STATE_DOWN and
            conns.cur_idle_connections > 0 then
            if max_conns == -1 or
                s.connected_clients < max_conns then
                max_conns = s.connected_clients
                max_conns_ndx = index
            end
        end
        index = index + 1
    end

    if max_conns_ndx > 0 then
        proxy.connection.backend_ndx = max_conns_ndx
        proxy.global.balance.query_index = max_conns_ndx + 1
    else
        --proxy.global.balance.query_index = 2
        proxy.global.balance.query_index = math.random(1, num)
    end
end

-- read/write splitting sends all non-transactional SELECTs to the
-- slaves is_in_transaction tracks the state of the transactions
local is_in_transaction            = false

-- if this was a SELECT SQL_CALC_FOUND_ROWS, stay on the same connections
local is_in_select_calc_found_rows = false

function connect_server()
    -- get a connection to a backend
    -- as long as we don't have enough connections in the pool,
    -- create new connections. make sure that we connect to each
    -- backedn at least once to keep the connections to the servers
    -- alive.
    -- on read_query we can switch the backend to another backend.

    ---[=[ @@LOG
    log.info("[conenct_server]")
    --]=]

    local least_idle_conns_ndx = 0
    local least_idle_conns     = 0
    local index = proxy.global.balance.connect_index
    local num = #proxy.global.backends

    for j = 1, num do
        if index > num then index = 1 end
        local s = proxy.global.backends[index]
        local pool = s.pool
        local cur_idle = pool.users[""].cur_idle_connections

        ---[=[ @@LOG
        log.debug("  backend_server_name   = " .. s.dst.name)
        log.debug("  connected_clients     = " .. s.connected_clients)
        log.debug("  idling_connections    = " .. cur_idle)
        log.debug("  type                  = " .. s.type)
        log.debug("  state                 = " .. s.state)
        --]=]

        if s.state ~= proxy.BACKEND_STATE_DOWN then
            -- try to connect to each backend once at least
            if cur_idle == 0 then
                proxy.connection.backend_ndx = index
                proxy.global.balance.connect_index = index + 1
                ---[=[ @@LOG
                log.info("server[" .. s.dst.name ..
                         "][first] open new conneciton")
                --]=]
                return
            end

            -- try to open at least min_idle_connections
            if least_idle_conns_ndx == 0 or
                ( cur_idle < min_idle_connections and
                cur_idle < least_idle_conns ) then
                least_idle_conns_ndx = index
                least_idle_conns = cur_idle
            end
        end
        index = index + 1
    end

    if least_idle_conns_ndx > 0 then
        proxy.global.balance.connect_index = index
        proxy.connection.backend_ndx = least_idle_conns_ndx
        local s = proxy.global.backends[proxy.connection.backend_ndx]
        local pool = s.pool
        local cur_idle = pool.users[""].cur_idle_connections

        if cur_idle >= min_idle_connections then
            ---[=[ @@LOG
            log.info("server[" .. s.dst.name .."] using pooled connection")
            --]=]
            return proxy.PROXY_IGNORE_RESULT
        end
    else
        --will never happen
        print("[error] least_idle_conns_ndx <= 0 in connect_server")
        assert(false)
    end
    -- open a new connection
    ---[=[ @@LOG
    log.info("server[" ..
    proxy.global.backends[proxy.connection.backend_ndx].dst.name ..
    "] open new conneciton")
    --]=]
end


--[=[ @@DEBUG Obtain information of read_handshake
function read_handshake()
log.info("[read_handshake]")
log.debug("  mysqld-version     = " .. proxy.connection.server.mysqld_version)
log.debug("  thread-id          = " .. proxy.connection.server.thread_id)
log.debug("  scramble-buf       = " .. string.format("%q",proxy.connection.server.scramble_buffer))
log.debug("  server-addr        = " .. proxy.connection.server.dst.name)
log.debug("  client-addr        = " .. proxy.connection.client.dst.name)
end
--]=]


function read_auth_result(auth)
    -- put the successfully authed connection into the connection pool
    -- @param auth the context information for the auth
    -- auth.packet is the packet

    ---[=[ @@LOG
    log.info("[read_auth_result]")
    --]=]

    if auth.packet:byte() == proxy.MYSQL_PACKET_OK then
        --auth was fine, disconnect from the server
        proxy.connection.backend = 0
        ---[=[ @@LOG
    elseif auth.packet:byte() == proxy.MYSQLD_PACKET_EOF then
        -- we received either a
        -- * MYSQLD_PACKET_ERR and the auth failed or
        -- * MYSQLD_PACKET_EOF which means a OLD PASSWORD (4.0) was sent
        log.debug("  read_auth_result ... not ok yet");
    elseif auth.packet:byte() == proxy.MYSQLD_PACKET_ERR then
        log.debug("  auth failed!")
        --]=]
    end
end

function read_query(packet)
    ---[=[ @@LOG
    log.info("[read_query]")
    --]=]

    local cmd = commands.parse(packet)
    local c   = assert(proxy.connection.client)

    --[=[ @@OPTION internal command
    -- TODO maybe can use auto_config.handle in admin
    local res = auto_config.handle(cmd)
    if res then
    log.debug("  authed backend     = " .. proxy.connection.backend_ndx)
    return res
    end
    --]=]

    ---[=[ @@LOG
    log.debug("  command name       = " .. cmd.type_name)
    log.debug("  authed backend     = " .. proxy.connection.backend_ndx)
    log.debug("  used db            = " .. proxy.connection.client.default_db)
    log.debug("  client username    = " .. proxy.connection.client.default_db)
    if cmd.type == proxy.COM_QUERY then
        log.debug("  query              = " .. cmd.query)
    end
    --]=]

    if cmd.type == proxy.COM_QUIT
        or cmd.type == proxy.COM_PING then
        -- don't send COM_QUIT to the backend. We manage the connection
        -- in all aspects.
        proxy.response = {
            type = proxy.MYSQLD_PACKET_OK,
        }
        ---[=[ @@LOG
        log.info("client quit");
        --]=]
        return proxy.PROXY_SEND_RESULT
    end

    proxy.queries:append(1, packet, { resultset_is_needed = true })

    -- read/write splitting
    -- send all non-transaction SELECTs to slaves
    if not is_in_transaction then
        if cmd.type == proxy.COM_QUERY then
            local tokens = tokens or assert(tokenizer.tokenize(cmd.query))
            local stmt = tokenizer.first_stmt_token(tokens)

            if stmt.token_name == "TK_SQL_SELECT" then
                in_in_select_calc_found_rows = false
                local is_insert_id = false

                for i = 1, #tokens do
                    local token = tokens[i]
                    --[=[ @@LOG
                    log.debug("  token: " .. token.token_name)
                    log.debug("  val  : " .. token.text)
                    --]=]

                    -- SQL_CALC_FOUND_ROWS + FOUND_ROWS() have to be
                    -- executed on the same connection
                    if not is_in_select_calc_found_rows
                        and token.token_name == "TK_SQL_SQL_CALC_FOUND_ROWS" then
                        is_in_select_calc_found_rows = true

                    elseif not is_insert_id and
                        token.token_name == "TK_LITERAL" then

                        local utext = token.text:upper()
                        if utext == "LAST_INSERT_ID" or
                            utext == "@@INSERT_ID" then
                            is_insert_id = true
                        end
                    end

                    -- found the two special token, can not find more
                    if is_insert_id and is_in_select_calc_found_rows then
                        break
                    end
                end

                -- if we ask for the last-insert-id we have to ask it on
                -- the original connection
                if not is_insert_id then
                    idle_ro()
                ---[=[ @@LOG
                else
                    log.info("found a SELECT LAST_INSERT_ID()," ..
                    "staying on the same backend")
                --]=]
                end

            -- prohibit modification of data because we only want to proxy slaves
            --[[ rw-spliting
            elseif stmt.token_name == "TK_SQL_UPDATE"
                or stmt.token_name == "TK_SQL_DELETE"
                or stmt.token_name == "TK_SQL_CREATE"
                or stmt.token_name == "TK_SQL_DROP"
                or stmt.token_name == "TK_SQL_RENAME"
                or stmt.token_name == "TK_SQL_INSERT"
                or stmt.token_name == "TK_SQL_ALTER" then

                proxy.response = {
                    type = proxy.MYSQLD_PACKET_ERR,
                    errmsg = "on slave, you can not write"
                }
                return proxy.PROXY_SEND_RESULT
            else
                ---[=[ @@LOG
                log.debug("undefine query, also choose slave")
                --]=]
                idle_ro()
            end
            --]]
        elseif cmd.type == proxy.COM_INIT_DB then
            -- TODO: @need search or cmd.type == proxy.COM_FIELD_LIST then
            -- init DB can execute on slave

            idle_ro()
        end
        --[[ rw-spliting
        else
            -- prohibit other type
            proxy.response = {
                type = proxy.MYSQLD_PACKET_ERR,
                errmsg = "do not support type " .. cmd.type_name
            }
            print("do not supprt type " .. cmd.type_name)
            return proxy.PROXY_SEND_RESULT
        end
        --]]
    else
        --TODO in transaction
        if proxy.connection.backend_ndx == 0 then
            log.error("in transation, but do not have a assigned backend")

            proxy.response = {
                type = proxy.MYSQLD_PACKET_ERR,
                errmsg = "in transation, no a assigned backend"
            }
            return proxy.PROXY_SEND_RESULT
        end
    end

    -- no backend selected yet, pick a master
    if proxy.connection.backend_ndx == 0 then
        proxy.connection.backend_ndx = idle_failsafe_rw()
    end

    if proxy.connection.backend_ndx == 0 then
        -- Maybe the master is down
        -- OR NO AVAILABLE CONNECTIONS
        -- !!!Very disappointed:( MySQL proxy can not connect server in read_query
        -- we have to return err packet.
        log.error("query need master, and master is down")
        --]=]

        proxy.response = {
            type = proxy.MYSQLD_PACKET_ERR,
            errmsg = "no available connections"
        }
        return proxy.PROXY_SEND_RESULT
    end

    local s = assert(proxy.connection.server)
    -- if client and server db don't match, adjust the server-side
    -- skip it if we send a INIT_DB anyway
    if cmd.type ~= proxy.COM_INIT_DB and
        c.default_db and c.default_db ~= s.default_db then
        ---[=[ @@LOG
        log.debug("  server default db  = " .. s.default_db)
        log.debug("  client default db  = " .. c.default_db)
        log.debug("  database syncronizing")
        --]=]
        proxy.queries:prepend(2, string.char(proxy.COM_INIT_DB) .. c.default_db)
    end

    ---[=[ @@LOG
    if proxy.connection.backend_ndx > 0 then
        local b = proxy.global.backends[proxy.connection.backend_ndx]
        log.debug("  authed backend     = " .. proxy.connection.backend_ndx)
        log.debug("  sending to backend = " .. b.dst.name);
        log.debug("  is_slave           = " .. tostring(b.type == proxy.BACKEND_TYPE_RO));
        log.debug("  server default db  = " .. s.default_db)
        log.debug("  server username    = " .. s.username)
    end
    log.debug("  server username    = " .. s.username)
    log.debug("  in_trans           = " .. tostring(is_in_transaction))
    log.debug("  in_calc_found      = " .. tostring(is_in_select_calc_found_rows))
    log.debug("  COM_QUERY          = " .. tostring(cmd.type == proxy.COM_QUERY))
    --]=]
    return proxy.PROXY_SEND_QUERY
end


function read_query_result(inj)
    -- as long as we are in a transaction keep the connection
    -- otherwise release it so another client can use it

    ---[=[ @@LOG
    log.info("[read_query_result]")
    --]=]
    local res = assert(inj.resultset)
    if inj.id ~= 1 then
        -- ignore the result of the USE <default_db>
        if inj.id == 2 then
            -- the injected INIT_DB failed as the slave doesn't have this DB
            -- or doesn't have permissions to read from it
            if res.query_status == proxy.MYSQLD_PACKET_ERR then
                proxy.queries:reset()

                proxy.response = {
                    type = proxy.MYSQLD_PACKET_ERR,
                    errmsg = "can't change DB "..
                    proxy.connection.client.default_db ..
                    " to on slave " ..
                    proxy.global.backends[
                    proxy.connection.backend_ndx
                    ].dst.name
                }
                return proxy.PROXY_SEND_RESULT
            end
            return proxy.PROXY_IGNORE_RESULT
        end
    end

    is_in_transaction = res.flags.in_trans
    local have_last_insert_id = (res.insert_id and (res.insert_id > 0))

    if not is_in_transaction and
        not is_in_select_calc_found_rows and
        not have_last_insert_id then
        -- release the backend
        proxy.connection.backend_ndx = 0
    ---[=[ @@LOG
    else
        log.debug("  COM_QUERY          = " .. tostring(cmd.type == proxy.COM_QUERY))
        log.debug("staying on the same backend")
        log.debug("  in_trans           = " .. tostring(is_in_transaction))
        log.debug("  in_calc_found      = " .. tostring(is_in_select_calc_found_rows))
        log.debug("  have_insert_id     = " .. tostring(have_last_insert_id))
    --]=]
    end
end


function disconnect_client()
    -- close the connections if we have enough connections in the pool
    -- @return nil           close connection
    -- @return IGNORE_RESULT store connection in the pool

    ---[=[ @@LOG
    log.info("[disconnect_client]")
    --]=]

    if proxy.connection.backend_ndx == 0 then
        -- client doesn't have a server backend assigned
        -- pick a server which has too many idling connections and close one
        local index = proxy.global.balance.disconnect_index
        local num = #proxy.global.backends

        for j = 1, num do
            if index > num then index = 1 end
            local s = proxy.global.backends[index]
            local pool = s.pool
            local cur_idle = pool.users[proxy.connection.client.username].cur_idle_connections

            if s.state ~= proxy.BACKEND_STATE_DOWN and
                cur_idle > max_idle_connections then
                -- try to disconnect a backend
                ---[=[ @@LOG
                proxy.connection.backend_ndx = index
                proxy.global.balance.disconnect_index = index + 1
                log.info("  " .. s.dst.name ..
                         " closing connection, idling: " .. cur_idle)
                --]=]
                return
            end
            index = index + 1
        end
        --proxy.global.balance.disconnect_index = 2
        proxy.global.balance.disconnect_index = math.random(1, num)
        return proxy.PROXY_IGNORE_RESULT
        -- else
        --     --client has a server backend assigned, maybe in transaction
        --     --it is better disconnect directly
        --     return
    end
end
